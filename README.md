## Cấu trúc project:
- Nên để trong folder riêng cho: file source (```.c/.h```), file project (vd ```CCS/MPLab...```), file _build_ output (```lst, map, hex, bin...```)
- Exclude git đối với _build_ folder/files, ```.vscode```
- File tài liệu dạng word/excel... tốt nhất không để trong git vì nó là dạng binary, lỡ merge/conflict hay có vấn đề gì là hỏng luôn format, đi luôn file không mở lên dc. Tốt nhất share lên google drive.
- Tìm hiểu markdown để viết ```README.md``` theo chuẩn  
## Source code:  
- Nhất quán (persistency): vd define các pin (```SSR, switch_water```) nếu là UPPER_CASE/lower_case thì giữ luôn 1 loại như thế, đừng mix! 
- Don't repeat yourself & don't hardcode: thay vì  
  ```output_low(SSR)``` bên trong chương trình, thì nên define tường minh  
  ```#define SRR_OFF() output_low(SSR)```  rồi s/d ```SSR_OFF``` bên trong chương trình
  Làm vậy thì có review/maintenance code về sau cũng đỡ vã, và có sửa phần cứng (SSR kích low/high active) j cũng chỉ cần sửa đoạn define thôi k fải đục chương trình sửa từng dòng.  
- *Debounce* trong ```water_level_low()``` chưa đúng: debounce nghĩa là quá trình sampling trong thời gian timeout cho trước, cứ interval check, mỗi lần check nếu state đổi thì reset timeout & save state, ngược lại nếu state const thì check timeout, nếu timeout thì return state đó.
- ```temp_is_high()``` s/d comparator cũng cần debounce tương tự như trên.
- Check lại lưu đồ, block condition chỉ có 1 vào và 2 ra tương ứng với thỏa/không thỏa.
  ![Condition](Condition.png)  
- Lưu đồ giải thuận nên convert lại theo state-machine chuẩn sau này có phức tạp hơn thì cũng dễ maintenance, implement bằng switch-case trong vòng lặp lớn, đề xuất:  
  - STATE_INIT: state khởi tạo, check các điều kiện (mực nước, nhiệt độ) để entry vào 1 trong các states bên dưới, có thể tương ứng với đoạn này:  
  ![Init state](state_init.png)  
  - STATE_ON: trong state này vẫn check các điều kiện (mực nước, nhiệt độ) để chuyển sang STATE_ERR hoặc check timeout (_ON) để SSR_OFF và chuyển sang STATE_OFF. Có thể tương ứng với đoạn này:  
  ![On state](state_on.png)  
  - STATE_OFF: trong state này check timeout (_OFF) để SSR_ON lại và chuyển sang STATE_ON (hoặc kỹ hơn là check các điều kiện mực nước, nhiệt độ để chuyển sang STATE_ON hoặc STATE_ERR). Có thể tương ứng với đoạn này:  
  ![Off state](state_on.png)  
  - STATE_ERR: infinite loop, chớp tắt led j đó:  
  ![Error state](state_err.png)  
